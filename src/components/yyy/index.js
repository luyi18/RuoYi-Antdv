import YTableSearchCreator from './YTableSearchCreator.vue'
import YTimeRangePicker from './YTimeRangePicker.vue'
import YDictSelectTag from './dict/YDictSelectTag.vue'
import YDrawer from './YDrawer.vue'

require("./vxeComponents/vxeRender")
export default {
  install(Vue) {
    Vue.component('YDrawer', YDrawer)
    Vue.component('YDictSelectTag', YDictSelectTag)
    Vue.component('YTimeRangePicker', YTimeRangePicker)
    Vue.component('YTableSearchCreator', YTableSearchCreator)
  }
}