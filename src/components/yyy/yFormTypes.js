export const YFormTypes = {
  // 输入框
  input: 'input',
  // 选择器
  select: 'select',
  // 字典选择器
  dictSelect: 'dict_select',
  // 时间选择器
  time: 'time',
  // 时间范围选择器
  timeRange: 'time_range'
}
export default YFormTypes
