import VXETable from 'vxe-table'
import VxeOpButtons from './VxeOpButtons.vue'


VXETable.renderer.add('opButtons', {
  // 默认显示模板
  renderDefault(h, renderOpts, params) {
    let { row, column } = params
    let { buttonExampleClick, buttonList } = renderOpts
    if (buttonList.length <= 0) {
        return null
    }
    return [<VxeOpButtons ref={'VxeOpButton_id' + row.id} list={buttonList} row={row} onClick={(event) => buttonExampleClick(event, row)}></VxeOpButtons>]
  },
})
