import { filterObj } from '@/utils/util';
import XEUtils from 'xe-utils'
import { deleteAction, getAction,downFile} from '@/api/common'
import Vue from 'vue'
import { UI_CACHE_DB_DICT_DATA } from '@/store/mutation-types'
const rowHeightMap = {
  small: 35,
  medium: 44,
  normal: 55,
}
const DefaultHeightType = 'small'
const OpColField = 'vxe_custome_op_'
const  tableFieldColors = [
  // {
  //   itemBackground: "#f00",
  //   itemColor: "#fff",
  //   itemText: "审核状态",
  //   itemValue: "已审核"
  // },
  // {
  //   itemBackground: "#f00",
  //   itemColor: "#fff",
  //   itemText: "审核状态",
  //   itemValue: "已驳回"
  // }
]
export const YVxeListMixin = {
  data() {
    return {
      rowHeightType: DefaultHeightType,
      isFullScreen: false,
      // vxetable配置
      gridOptions: {
        id: '',
        scrollY: {
          gt: 30,
          mode : 'wheel'
        },
        loading: true,
        size: 'mini',
        class: 'sortable-vxetable-column',
        border: true,
        resizable: true,
        autoSize: true,
        showOverflow: true,
        height: 500,
        columnConfig: {
          useKey: true, // 拖拽切换顺序，需要这个key
          minWidth: 100
        },
        pagerConfig: {
          enabled:true,
          background: true,
          total: 0,
          currentPage: 1,
          pageSize: 50,
          layouts: ['PrevJump', 'PrevPage', 'JumpNumber', 'NextPage', 'NextJump', 'Sizes', 'FullJump', 'Total'],
          pageSizes: [50, 100, 200, 500, 1000]
        },
        data: [],
        columns: [],
        checkboxConfig: {
          checkField: 'checked',
          //labelField: 'id',
          trigger: 'cell',
          reserve: true,
          highlight: true,
          range: false
        },
        rowConfig: {
          keyField: 'id',
          isHover: true,
          height: rowHeightMap[DefaultHeightType],
          isCurrent: true
        },
        strip: true,
        footerMethod: this.footerMethod,
        footerCellClassName: this.footerCellClassName,
        rowStyle: this.rowStyle,
        // showFooter:true,
        // 是否显示checkbox
        showCheck: true,
        // 是否显示序列
        showSeq: true
      },
      // 查询参数
      queryParam: {},
      // 默认查询条件，可以填一些默认值
      defaultQueryParam: {},
      // 排序参数
      isorter: {
        column: 'createTime',
        order: 'desc'
      },
      // 授权按钮列表
      buttonList: [],
      lockUrl: {
        queryByDictCode: "/base/baseTenantDict/queryByDictCode",
      },
      /* table选中keys*/
      selectedRowKeys: [],
      /* table选中records*/
      selectionRows: [],
      // 需要计算合计值的列
      tableSum: [],
      tableKey: 0,
      // 初始化定义列的地方
      defColumns: [],
      // 状态控制
      // 表格高度自适应
      tableSelfAdaption: true,
      // 自定义offsetValue,调整这个达到表格占满页面
      offsetValue: 220,
      // 满屏状态下的offset
      fullscreenOffset: 60,
       // 自己设置的，优先级最高
       operationWidth: 0,
      // 操作列的宽度-动态计算的
      operationCalculatorWidth: 0,
      // 颜色示例
      tableExampleList: [],
      // 自定义行的颜色(在每个)
      fieldStyle: [],
    }
  },
  created() {
    let userInfo = this.$store.getters.userInfo
    // 设置表格id--用户隔离
    this.gridOptions.id = `vxetable_${this.$options.name}_${userInfo.userId}`

     // 动态计算操作列的宽度
    if (this.buttonList && this.buttonList.length > 0) {
      this.calculatorOperationColumnWidth()
    }
    
  },
  mounted () {
    // 计算表格高度
    this.calculationHeight()
    window.onresize = () => {
      this.calculationHeight()
    }   
    // if (this.toolbar) {
    //   this.addToolbar()
    // }
  },
  methods: {
    getTableFieldColour(){
      return new Promise((resolve) => {
        setTimeout(() => {
          let styleList = []
          tableFieldColors.forEach((item) => {
            let columnsIndex = this.defColumns.findIndex(columnsItem => columnsItem.title == item.itemText)
            if (columnsIndex > 0) {
              let column = this.defColumns[columnsIndex]
              let style = {}
              style.itemBackground = item.itemBackground
              style.itemColor = item.itemColor
              style.itemValue = item.itemValue
              style.itemText = column.field
              styleList.push(style)
            }
          })
          this.fieldStyle = styleList
          resolve()
        }, 100)
      })
    },
    addToolbar() {
      var container = this.$el.querySelector('.op-right')
      if (container) {
        var that = this
        const component = new Vue({
          render(h) {
            return (
              <j-vxe-tool-bar rowHeightMode={that.rowHeightType} columns={that.gridOptions.columns} onChange={e => {
                that.onColSettingsChange(e)
              }} style={that.toolbar.style}></j-vxe-tool-bar>
            )
          }
        }).$mount();
        container.appendChild(component.$el);
      }
    },
    setRowHeight(type) {
      this.rowHeightType = type
      this.gridOptions.rowConfig.height = rowHeightMap[type]
      this.saveConfig({
        rowHeight: type
      })
    },
    async initMixin() {
      this.setColumns()
    //   try {
    //     // 获取表格颜色配置
    //     await this.getTableFieldColour()
    //   } catch (error) {
    //     console.error("DecorateVxeTableMixin error", error);
    //   }
      this.loadData()
    },
    setColumns() {
      // 获取所有列配置
      let localVxe = Vue.ls.get(this.gridOptions.id)
      if (localVxe && localVxe.config && localVxe.config.rowHeight) {
        this.rowHeightType = localVxe.config.rowHeight
        this.gridOptions.rowConfig.height = rowHeightMap[this.rowHeightType]
      }
      this.gridOptions.columns = this.loadSettingColumns()
    },
    loadSettingColumns() {
      const cols = []
      cols.push({
        type: 'checkbox',
        width: 50,
        fixed: 'left',
        resizable: false,
        visible: this.gridOptions.showCheck,
      })
      cols.push({
        type: 'seq',
        width: 60,
        title: '序号',
        fixed: 'left',
        align: "center",
        resizable: false,
        visible: this.gridOptions.showSeq,
      })
      let operation = {
        title: '操作',
        align: "left",
        fixed: 'left',
        field: OpColField,
        resizable: true,
        visible: true,
        cellRender: {
          name: 'opButtons',
          buttonList: this.buttonList,
          buttonExampleClick: this.buttonExampleClick
        }
      }
      let localVxe = Vue.ls.get(this.gridOptions.id)
      if (!localVxe || !localVxe.cols) {
        // 不存在本地缓存的时候
        if (this.buttonList && this.buttonList.length > 0) {
          // 重置表格的时候，会用到
          if (this.operationCalculatorWidth) {
            operation.width = this.operationCalculatorWidth
          }
          // 如果自己设置了，用自己的
          if (this.operationWidth) {
            operation.width = this.operationWidth
          }
          cols.push(operation)
        }
        // 加一个visible的配置
        this.defColumns.forEach(item => {
          item.visible = item.visible !== undefined ? item.visible : true
        })
        cols.push(...this.defColumns)
      }else {
        // 处理勾选列
        const checkboxColumn = localVxe.cols.find(c => c.type == 'checkbox')
        if (checkboxColumn) {
          cols[0].visible = checkboxColumn.visible
        }
        // 处理 序号列
        const seqColumn = localVxe.cols.find(s => s.type == 'seq')
        if (seqColumn) {
          cols[1].visible = seqColumn.visible
        }
        // 处理 操作列
        const opColumn = localVxe.cols.find(o => o.field == OpColField)
        if (opColumn && this.buttonList && this.buttonList.length > 0) {
          cols.push({...operation,...opColumn})
        }
        // 处理defColumn
        const statusDefColumns = []

        this.defColumns.forEach(d => {
          // 加载本地配置
          var localItem = localVxe.cols.find(l => l.field && d.field &&  d.field === l.field)
          // 将localItem的配置加载覆盖到defColumn中
          // 用代码里的title，不要用本地的
          statusDefColumns.push(Object.assign({},d,localItem,{
            title: d.title
          }))
        })
        cols.push(...statusDefColumns)
      }
      // 统计需要合计的列
      this.tableSum = this.getSumCol(cols)
      return cols;
    },
    // 注意这个row不能少
    buttonExampleClick(btn,row) {
      eval(btn.BtnScript)
    },
    getSumCol(arr) {
      var footerArr = []
      for (var item of arr) {
        if (item.showFooter) {
          if (item.children) {
            footerArr = footerArr.concat(this.getSumCol(item.children))
          } else {
            footerArr.push(item.field)
          }
        }
      }
      return footerArr
    },
    loadData(arg,cb) {
      if (!this.url || !this.url.list) {
        this.$message.error('请设置url.list属性')
        return
      }
      if (arg === 1) {
        this.gridOptions.pagerConfig.currentPage = 1
      }
      var params = this.getQueryParams()
      this.gridOptions.loading = true

      getAction(this.url.list,params).then(res => {
          let recordData = res.rows
          this.gridOptions.data = recordData
          if (res.total) {
            this.gridOptions.pagerConfig.total = res.total
          }else {
            this.gridOptions.pagerConfig.total = 0
          }
          cb && cb()
      }).catch((error) => {
        this.gridOptions.data = []
        this.$message.warning(error.message)
      }).finally(() => {
        this.gridOptions.loading = false
      })
    },
    handleTableChange({currentPage,pageSize}) {
      this.gridOptions.pagerConfig.pageSize = pageSize
      this.gridOptions.pagerConfig.currentPage = currentPage
      this.loadData()
    },
    handleDetail(record) {
      this.$refs.modalForm.edit(record);
      this.$refs.modalForm.title = "详情";
      this.$refs.modalForm.disableSubmit = true;
    },
    handleEdit: function(record) {
      this.$refs.modalForm.edit(record);
      this.$refs.modalForm.title = "编辑";
      this.$refs.modalForm.disableSubmit = false;
    },
    handleAdd: function() {
      this.$refs.modalForm.add();
      this.$refs.modalForm.title = "新增";
      this.$refs.modalForm.disableSubmit = false;
    },
    batchDel: function() {
      if (!this.url.deleteBatch) {
        this.$message.error("请设置url.deleteBatch属性!")
        return
      }

      if (this.selectedRowKeys.length <= 0) {
        this.$message.warning('请选择一条记录！');
        return;
      } else {
        var ids = "";
        for (var a = 0; a < this.selectedRowKeys.length; a++) {
          ids += this.selectedRowKeys[a] + ",";
        }
        var that = this;
        this.$confirm({
          title: "确认删除",
          content: "是否删除选中数据?",
          onOk: function() {
            that.loading = true;
            deleteAction(that.url.deleteBatch, {
              ids: ids
            }).then((res) => {
              if (res.success) {
                //重新计算分页问题
                that.reCalculatePage(that.selectedRowKeys.length)
                that.$message.success(res.message);
                that.loadData();
                that.onClearSelected();
              } else {
                that.$message.warning(res.message);
              }
            }).finally(() => {
              that.loading = false;
            });
          }
        });
      }
    },
    handleDelete: function(id) {
      if (!this.url.delete) {
        this.$message.error("请设置url.delete属性!")
        return
      }
      var that = this;
      deleteAction(that.url.delete, {
        id: id
      }).then((res) => {
        if (res.success) {
          //重新计算分页问题
          that.reCalculatePage(1)
          that.$message.success(res.message);
          that.loadData();
        } else {
          that.$message.warning(res.message);
        }
      });
    },
    reCalculatePage(count) {
      //总数量-count
      let total = this.gridOptions.pagerConfig.total - count;
      //获取删除后的分页数
      let currentIndex = Math.ceil(total / this.gridOptions.pagerConfig.pageSize);
      //删除后的分页数<所在当前页
      if (currentIndex < this.gridOptions.pagerConfig.currentPage) {
        this.gridOptions.pagerConfig.currentPage = currentIndex;
      }
    },
    modalFormOk() {
      // 新增/修改 成功时，重载列表
      this.loadData();
      //清空列表选中
      this.onClearSelected()
    },
    searchQuery() {
      this.loadData(1);
    },
    searchReset(initData) {
      this.queryParam = initData || {}
      this.loadData(1);
      this.selectedRowKeys = []
      this.selectionRows = []
    },
    selectChangeEvent() {
      this.selectedRowKeys = []
      this.selectionRows = []
      const records = this.$refs.xGrid.getCheckboxRecords()
      if (records.length > 0) {
        for (var a = 0; a < records.length; a++) {
          this.selectedRowKeys.push(records[a][this.gridOptions.rowConfig.keyField || 'id'])
          this.selectionRows.push(records[a])
        }
      }
    },
    onClearSelected() {
      this.selectedRowKeys = [];
      this.selectionRows = [];
      this.$refs.xGrid.clearCheckboxRow()
    },
    handleExportXls(fileName,url,extra = {}) {
      if (!fileName || typeof fileName != "string") {
        fileName = "导出文件"
      }
      let param = this.getQueryParams();
      param = Object.assign({},param,extra)
      if (this.selectedRowKeys && this.selectedRowKeys.length > 0) {
        param['selections'] = this.selectedRowKeys.join(",")
      }
      console.log("导出参数", param)
      downFile(url || this.url.exportXlsUrl, param).then((data) => {
        if (!data) {
          this.$message.warning("文件下载失败")
          return
        }
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(new Blob([data], {
            type: 'application/vnd.ms-excel'
          }), fileName + '.xls')
        } else {
          let url = window.URL.createObjectURL(new Blob([data], {
            type: 'application/vnd.ms-excel'
          }))
          let link = document.createElement('a')
          link.style.display = 'none'
          link.href = url
          link.setAttribute('download', fileName + '.xls')
          document.body.appendChild(link)
          link.click()
          document.body.removeChild(link); //下载完成移除元素
          window.URL.revokeObjectURL(url); //释放掉blob对象
        }
      })
    },
    calculationHeight() {
      if (this.tableSelfAdaption) {
        let searchHeight = 0
        let opHeight = 0
        if (this.$refs.getSearchHeight) {
          searchHeight = this.$refs.getSearchHeight.offsetHeight
        }
        if (this.$refs.getOperateHeight) {
          opHeight = this.$refs.getOperateHeight.offsetHeight
        }
        var offsetValue = this.isFullScreen ? this.fullscreenOffset : this.offsetValue
        this.gridOptions.height = document.body.clientHeight - searchHeight - offsetValue - opHeight
      }
    },
    calculatorOperationColumnWidth() {
      // 添加锁，防止调用多次
      let widthLock = false;
      // 监听操作列渲染完成事件
      this.$bus.$once('vxe-op-button-rendered',width => {
        console.log("pppppppppppppp",width);
        if (!this.$refs.xGrid) {
          return
        }
        if (width && !widthLock) {
          widthLock = true
          const { collectColumn } = this.$refs.xGrid.getTableColumn()
          
          let operatCol = collectColumn.find(item => item.field == OpColField)
          if (!operatCol || operatCol.width) {
            return
          }
          operatCol.width = width + 20 + 15
          // 保存宽度(重置的时候需要)
          this.operationCalculatorWidth = operatCol.width
          this.$refs.xGrid.refreshColumn()
        }
        console.log("tttttttttttttt",width);
        // this.$bus.$once('vxe-button-example-rendered')
      })
    },
    getQueryParams() {
      //获取查询条件
      let sqp = {}
      var param = Object.assign(sqp, this.isorter, this.defaultQueryParam,this.queryParam,);
    //   param.field = this.getQueryField();
      param.pageNo = this.gridOptions.pagerConfig.currentPage;
      param.pageSize = this.gridOptions.pagerConfig.pageSize;
      return filterObj(param);
    },
    getQueryField() {
      //TODO 字段权限控制
      var str = "id,";
      this.gridOptions.columns.forEach(function(value) {
        if (value.field) {
          str += "," + value.field;
        }
      });
      return str;
    },
    onFullScreen(value) {
      this.isFullScreen = value
      if (this.isFullScreen) {
        this.$el.classList.add('vxetable-fullscreen')
      }else {
        this.$el.classList.remove('vxetable-fullscreen')
      }
      this.$nextTick(() => {
        this.calculationHeight()
        if (this.isFullScreen) {
          this.stopScroll()
        }else {
          this.canScroll()
        }
      })
    },
    //禁止滚动
    stopScroll(){
        var mo=function(e){e.preventDefault();};
        document.body.style.overflow='hidden';
        document.addEventListener("touchmove",mo,false);//禁止页面滑动
    },
    /***取消滑动限制***/
    canScroll(){
        var mo=function(e){e.preventDefault();};
        document.body.style.overflow='';//出现滚动条
        document.removeEventListener("touchmove",mo,false);
    },
    /******  列的相关操作 */
    onColSettingsChange(e) {
      const { collectColumn } = this.$refs.xGrid.getTableColumn()
      const { row, checked,eventType,subType,oldItem,newItem,value } = e
      // 刷新事件
      if (eventType == 'refresh') {
        this.searchQuery()
        return
      }
      // 行高改变事件
      if (eventType == 'rowHeightModeChange') {
        this.setRowHeight(value)
        return
      }
      if (eventType == 'fullscreen') {
        this.onFullScreen(value)
        return
      }
      if (eventType == 'sort') {
        // 找到现在的排序
        const newIndex = collectColumn.findIndex(item => item.field == newItem.field)
        // 当oldItem不存在的时候，oldIndex为最后一个
        let oldIndex = collectColumn.length - 1
        // 说明是放到最后面
        if (oldItem) {
          oldIndex = collectColumn.findIndex(item => item.field == oldItem.field)
        }
        // 这个序号是没有包括第一列和第二列的
        this.sortColumnsByIndex(collectColumn,oldIndex,newIndex)
      }else if(eventType == 'fixed') {
         // 固定左/右
         collectColumn.forEach(item => {
          if (item.field == row.field) {
            item.fixed = row.fixed
          }
        })
        this.$refs.xGrid.refreshColumn()
      } else if(eventType == 'visible'){
        if (subType == 'checkbox') {
          // 是否要操作列
          collectColumn[0].visible = checked
          this.$refs.xGrid.refreshColumn()
        }else if(subType == 'seq') {
          // 是否要序号列
          collectColumn[1].visible = checked
          this.$refs.xGrid.refreshColumn()
        }else if(subType == 'single') {
          // 单选
          collectColumn.forEach(item => {
            if (item.field == row.field) {
              item.visible = checked
            }
          })
          this.$refs.xGrid.refreshColumn()
        }else if(subType == 'all') {
          if (checked) {
            // 全选
            collectColumn.forEach(item => {
              if (item.field) {
                item.visible = checked
              }
            })
            this.$refs.xGrid.refreshColumn()
          }else {
            // 全不选
            collectColumn.forEach(item => {
              if (item.field) {
                item.visible = checked
              }
            })
            this.$refs.xGrid.refreshColumn()
          }
        }
      }else if(eventType == 'reset') {
        this.resetVxeTable(collectColumn)
        return
      }
      this.$nextTick(() => {
        // 存储必要的状态信息
        this.saveColumns(collectColumn)
      })
    },
    // 将newItemIndex位置的元素，放到oldItemIndex的前面
    sortColumnsByIndex(fullColumn,oldItemIndex,newItemIndex) {
      // 移动到目标列
      const currColumn = fullColumn.splice(newItemIndex, 1)[0]
      fullColumn.splice(oldItemIndex, 0, currColumn)
      // 保存全量列
      this.$refs.xGrid.loadColumn(fullColumn)
    },
    // 拖动
    resizableChange(e) {
      const { collectColumn } = this.$refs.xGrid.getTableColumn()
      this.$nextTick(() => {
        // 存储必要的状态信息
        this.saveColumns(collectColumn)
      })
    },
    resetVxeTable() {
      Vue.ls.remove(this.gridOptions.id)
      setTimeout(() => {
        // 更改全部，重新设置列
        this.setColumns()
      }, 300);
    },
    // 保存列
    saveColumns(fields) {
      const saveColumns = fields.map(item => {
        return {
          title: item.title,
          field: item.field,
          visible: item.visible,
          type: item.type,
          fixed: item.fixed,
          width: item.resizeWidth || item.width
        }
      })
      const key = this.gridOptions.id
      let localVxe = Vue.ls.get(key)
      if (!localVxe) {
        localVxe = {}
      }
      localVxe.cols = saveColumns
      Vue.ls.set(key,localVxe)
    },
    // 保存其他配置
    saveConfig(config) {
      const key = this.gridOptions.id
      let localVxe = Vue.ls.get(key)
      if (!localVxe) {
        localVxe = {}
      }
      localVxe.config = Object.assign({},localVxe.config,config)
      Vue.ls.set(key,localVxe)
    },
    footerMethod(info) {
      var {
        columns,
        data
      } = info
      var sumArr = this.tableSum || []
      var footer = []

      if (sumArr.length > 0) {
        footer.push(columns.map((column, columnIndex) => {
          if (columnIndex === 0) {
            return '合计'
          } else{
            if (sumArr.includes(column.property)) {
              return  XEUtils.toFixed(XEUtils.sum(data, column.property),2)
            }
          }
          return '-'
        }))
      }
      return footer
    },
    footerCellClassName ({ $rowIndex, column, columnIndex }) {
      var sumArr = this.tableSum || []
      if (columnIndex === 0) {
        return 'bg-red'
      }
      if (sumArr.includes(column.property)) {
        return 'bg-red'
      }
    },
    rowStyle({
      row,
      rowIndex
    }) {
      let num = this.fieldStyle.findIndex(function(item, index) {
        return item.itemValue == row[item.itemText];
      });
      
      if (num > -1) {
        let example = this.fieldStyle[num]
       
        let exampleNum = this.tableExampleList.findIndex(function(exampleItem, index) {
          return exampleItem.itemValue == example.itemValue;
        });
        if (exampleNum == -1) {
          example.valueText = example.itemText.split('_')[0]
          example.value = row[example.itemText.split('_')[0]]
          this.tableExampleList.push(example)   
        }
        return {
          backgroundColor: this.fieldStyle[num].itemBackground,
          color: this.fieldStyle[num].itemColor
        }
      }
    },
    onStatusExampleClick(item) {
      console.log("iiiiiiiiiiiiiiiiiiiiiiiiiiiii",item);
      if (item.value) {
        this.queryParam[item.valueText] = item.value
        this.loadData()
      }
    },
    clearAll() {
      this.selectedRowKeys = []
      this.selectionRows = []
      this.$refs.xGrid.clearAll()
      this.gridOptions.data = []
    },
    /**
     * 设置过滤条件
     * @param {*} field,字段名 
     * @param {*} dictCode ,字段对应的字典
     * @returns 
     */
    setFieldFilterEvent(field,dictCode) {
      let isTrans = field.indexOf('dictText') !== -1
      const xTable = this.$refs.xGrid
      const column = xTable.getColumnByField(field)
      if (!column) {
          return null
      }
      let dicts = []
      // 获取字典
      let dictMap =  Vue.ls.get(UI_CACHE_DB_DICT_DATA) || {}
      dicts = dictMap[dictCode] || []
      dicts = dicts.map(item => {
          item.label = item.dictLabel
          if (isTrans) {
            item.value = item.dictLabel
          }else {
            item.value = item.dictValue
          }
          return item
      })
      // 修改筛选列表，并默认设置为选中状态
      xTable.setFilter(column, dicts)
      // 修改条件之后，需要手动调用 updateData 处理表格数据
      xTable.updateData()
    },
  },
  beforeDestroy() {
    if (this.columnSortable) {
      this.columnSortable.destroy()
    }
  }
}